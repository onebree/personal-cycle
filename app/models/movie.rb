class Movie < ActiveRecord::Base

  attr_accessible :title, :rating, :description, :release_date, :director

  def self.all_ratings
    %w(G PG PG-13 NC-17 R)
  end

  def same_director
    @movie = Movie.find(params[:id])
    if @movie.director.blank?
      flash[:notice] = "'#{@movie.title}' has no director listed."
      redirect_to movie_path(@movie) and return
    end
    @movies = Movie.find_movies_same_director(@movie)
  end
  
  def self.find_movies_by_same_director(movie)
    Movie.find_all_by_director(movie.director)
  end

end

