FactoryGirl.define do
  factory :movie do
    title         "Default Title"
    rating        "PG"
    director      "Default Director"
    release_date  {1.day.ago}
  end
end
