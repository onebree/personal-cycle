#HW4: BDD, TDD Cycle
In this assignment you will use a combination of Behavior-Driven Design (BDD) and Test-Driven Development (TDD) with the Cucumber and RSpec tools to add a "find movies with same director" feature to RottenPotatoes, and deploy the resulting app on Heroku.

##Setup
```shell
git clone https://github.com/saasbook/bdd-tdd-cycle
cd bdd-tdd-cycle/rottenpotatoes
bundle install --without production
bundle exec rake db:migrate
rails generate cucumber:install capybara
rails generate cucumber_rails_training_wheels:install
rails generate rspec:install
rake spec
rake cucumber
```

##Part 1: add a Director field to Movies
1. Create and apply a migration that adds the Director field to the movies table.
2. The director field should be a string containing the name of the movie’s director.
    - HINT: use `add_column` method of `ActiveRecord::Migration`
3. Once the migration is applied, run `rake db:test:prepare` to load post-migration schema.

##Part 2: use BDD+TDD to get new scenarios passing
We've provided [three Cucumber scenarios](http://pastebin.com/L6FYWyV7) to drive creation of the happy path of Search for Movies by Director.
1. Add director info to an existing movie
    - Modify existing views
    - Create new step definition
    - Possibly add to `features/support/paths.rb`
    - Not required: create new views or controller actions
2. Click new link "Find Movies With Same Director", and show all relevant movies 
    - Modify the existing Show Movie view
    - Add a route, view and controller method for "Find With Same Director"
3. Create sad paths, when the current movie has no director info

Going one Cucumber step at a time, use RSpec to create the appropriate controller and model specs to drive the creation of the new controller and model methods. At the least, you will need to write tests to drive the creation of: 
- a RESTful route for Find Similar Movies 
    + HINT: use the 'match' syntax for routes as suggested in "Non-Resource-Based Routes" 
in Section 4.1 of ESaaS
- a controller method to receive the click on "Find With Same Director", and grab the id (for example) of the movie that is the subject of the match (i.e. the one we're trying to find
movies similar to)
- a model method in the Movie model to find movies whose director matches that of the current movie
- SAD PATH: "no director" in either a controller _or_ model method

Add the following lines to the TOP of `spec/spec_helper.rb` and `features/support/env.rb`:
```ruby
require 'simplecov'
SimpleCov.start 'rails'
```

See the [ESaaS screencast](http://vimeo.com/34754907) for step-by-step instructions on setting up SimpleCov.

##Submission
Submit a `.tar.gz file with the following of your app:
- `app/`
- `config/`
- `db/migrate/`
- `features/`
- `spec/`
- `Gemfile`
- `Gemfile.lock`
- SimpleCov report files showing 90% or greater coverage for your models and controllers
- URI of your deployed app on Heroku
- Any files you modified
